#!/bin/bash

echo "!!! Startup procedure !!!"

database_connection_alive=1
discovery_connection_alive=1

check_connection() {
  return "$(nc -z $1 $2; echo $?)"
}

perform_connection_checks() {
  echo "Starting connection checks..."
  discovery_connection_alive="$(check_connection "discovery" "8761"; echo $?)"
  database_connection_alive="$(check_connection "db" "3306"; echo $?)"
  echo "Discovery state: $discovery_connection_alive :: Database state: $database_connection_alive"
}

perform_connection_checks
while [[ $database_connection_alive != 0 ]] | [[ $discovery_connection_alive != 0 ]]; do
  echo "Connection not alive. Retry in 5 sec."
  sleep 5
  perform_connection_checks
done

echo "Starting service... Procedure end."

java -jar resource.jar --spring.datasource.url=$DATASOURCE_URL \
            --spring.security.oauth2.resource.user-info-uri=$AUTH_USER_INFO_URI \
            --spring.security.oauth2.client.jwk.key-set-uri=$AUTH_KEY_SET_URI \
            --eureka.client.serviceUrl.defaultZone=$DISCOVERY_DEFAULT_ZONE
