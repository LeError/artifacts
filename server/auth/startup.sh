#!/bin/bash

echo "!!! Startup procedure !!!"

connection_alive=0

check_connection() {
  echo "Checking database connection..."
  local return_value="$(nc -z db 3306; echo $?)"
  echo "Host db:3306 status: $return_value"
  if [[ $return_value == 0 ]]
  then
    echo "Connection alive."
    connection_alive=1
  fi
}

while [ $connection_alive != 1 ]; do
  check_connection
  sleep 5
done

echo "Starting service... Procedure end."

java -jar auth.jar --spring.datasource.url=$DATASOURCE_URL
