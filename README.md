# PRP artifacts #

All artifacts from PRP repository builds are placed here. With the included Dockerfiles, you are able to build the whole PRP service system at ease.

## What you need ##

You need to have Docker and Docker Compose installed to get the system running. After installing both, open a terminal window and switch to the `server` path. Then, you just have to run

```
docker-compose up -d
```

to get everything up and running. Please notice, that you might need a `sudo` in front of the command.

## Troubleshoot ##
* startup.sh not found in Execution
    * Check if line endings are LF (unix) not CR LF (win)
